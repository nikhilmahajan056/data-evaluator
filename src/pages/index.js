import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useEffect, useState } from 'react'
import ProgressBar from './components/ProgressBar';
import { Chart } from "react-google-charts";

const inter = Inter({ subsets: ['latin'] })

// export const data = [
//   ["Status", "Total"],
//   ["Successful", 84],
//   ["Failed", 12],
//   ["Undefined", 4],
// ];

export const options = {
  title: "Data Evaluation",
};

export default function Home() {

  const [file, setFile] = useState("");
  const [loading, setLoading] = useState(false);
  const [allDatasets, setAllDatasets] = useState([]);
  const [response, setResponse] = useState();
  const [evaluationResult, setEvaluationResult] = useState([]);

  const getAllDatasets = async () => {
    fetch('/api/datasets').then(async (res) => {
      const response = await res.json();
      console.log("response:", response.datasets)
      setAllDatasets(response.datasets);
    })
  }

  useState(() => {
    console.log("allDatasets:", allDatasets)
  }, [allDatasets])

  useEffect(() => {
    if(loading) {
      setTimeout(() => {
        // setResponse(data)
        setLoading(false)
      }, 5000)
    }
  }, [loading])

  useEffect(() => {
    const getDatasets = async() => {
      await getAllDatasets();
    }
    getDatasets();
  }, [])

  const submitEvaluation = async(e) => {
    e.preventDefault();
    console.log("file upload clicked!", file);
    setLoading(true);
    const form = e.target;
    const formData = new FormData(form);
    console.log("formData:", formData)
    console.log("formData strings:",new URLSearchParams(formData).toString());
    // You can work with it as a plain object.
    const formJson = Object.fromEntries(formData.entries());
    console.log("formData json:",formJson);
    fetch('/api/evaluate', {
      method: 'POST',
      body: JSON.stringify(formJson),
    }).then(async (res) => {
      if (res.status === 200) {
        const response = await res.json();
        console.log(response)
        setEvaluationResult([
          ["Status", "Total"],
          ["Successful", response.successful],
          ["Failed", response.failed],
          ["Undefined", response.undefined],
        ])
        setResponse([
          ["Status", "Total"],
          ["Successful", response.successful],
          ["Failed", response.failed],
          ["Undefined", response.undefined],
        ])
        setLoading(false)
      } else {
        const response = await res.json();
        console.error(response)
        setLoading(false)
      }
    })
  }

  return (
    <>
        <div className={styles.grid}>
          <div
            className={styles.card}
          >
            {
              !response && (
                <>
                  <h2>
              Homepage
            </h2>
            <p>
              Evaluate your datasets here.
            </p>
            <form className={styles.fileUploadSection} onSubmit={(e) => submitEvaluation(e)}>
              <fieldset>
                <legend>Select your preferred LLMs(Large Language Models) option:</legend>
                <div>
                  <input type='radio' id='openAi' name='llms' value='openAi'></input>
                  <label htmlFor="openAi">OpenAI</label>
                  <input type='radio' id='tgi' name='llms' value='tgi'></input>
                  <label htmlFor='tgi'>Text Generation Inference(TGI)</label>
                </div>
              </fieldset>
              <div style={{display:"flex", flexDirection:"column", marginTop:"1rem", width:"34vw", height:"33vh"}}>
                <label>Choose a dataset:</label>
                <select name="selectedDataset">
                  {
                    allDatasets.length > 0 && allDatasets.map((dataset) => {
                      return (<option key={dataset._id} value={dataset.fullPath}>{dataset.fileName}</option>)
                    })
                  }
                </select>
                <label>Enter the prompt here:</label>
                <textarea id="promptText" name="promptText" style={{width:"100%", height:"100%"}}></textarea>
                <button type='submit' className={styles.submitFileUpload} disabled={loading}>Submit</button>
              </div>
            </form>
            {
              loading && (
                <ProgressBar
                    bgcolor="orange"
                    progress="30"
                    height={20}
                />
              )
            }
                </>
              )
            }
            {response && <Chart
              chartType="PieChart"
              data={evaluationResult}
              options={options}
              width={"100%"}
              height={"50vh"}
            />}
          </div>
        </div>
    </>
  )
}
