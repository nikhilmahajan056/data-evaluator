import Header from "./Header";
import styles from '@/styles/Home.module.css'

export default function App({children}) {

    return (
		<>
			<Header />
            <main className={styles.main}>{children}</main>
		</>
	);
}
