import { MongoClient } from 'mongodb'

export async function saveUploads(filename, path) {
    try {
        const uri = process.env.MONGODB_URI
        const client = new MongoClient(uri)
        console.log("client:", client)
        const db = client.db("testDEDB");

        const response = await db
            .collection("files")
            .insertOne({
                fileName: filename,
                fullPath: path
            })

        console.log("response:", response)
        client.close();
        return response.acknowledged;
    } catch (e) {
        console.error(e);
    }
}