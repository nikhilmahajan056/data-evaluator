import { MongoClient } from 'mongodb'

export async function saveEvaluationResultsToDB(fileId, llms, promptText, evaluationResults) {
    try {
        const uri = process.env.MONGODB_URI
        const client = new MongoClient(uri)
        const db = client.db("testDEDB");

        const response = await db
            .collection("results")
            .insertOne({
                fileId: fileId,
                llms: llms,
                promptText: promptText,
                evaluationResults: evaluationResults,
            })

        console.log("response:", response)
        client.close();
        return response.acknowledged;
    } catch (e) {
        console.error(e);
    }
}