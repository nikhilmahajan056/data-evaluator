import { MongoClient } from 'mongodb'

export async function getAllDatasets() {
    try {
        const uri = process.env.MONGODB_URI
        const client = new MongoClient(uri)
        // console.log("client:", client)
        const db = client.db("testDEDB");

        const response = await db
            .collection("files")
            .find()
            .toArray();

        const result = await response;
        // console.log("result:", result)
        client.close();
        return response;
    } catch (e) {
        console.error(e);
    }
}