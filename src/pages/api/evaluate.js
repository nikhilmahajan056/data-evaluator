import {saveEvaluationResultsToDB} from "../lib/saveEvaluationResultsToDB";

export default async function handler(req, res) {
    console.log("body is:", req.body)

  try {
    const {selectedDataset, llms, promptText} = JSON.parse(req.body);
    console.log(selectedDataset, llms, promptText)
    var min = 0;
    var max = 100;
    var successful = Math.floor(Math.random() * (max-min) + min);
    max -= successful;
    var failed = Math.floor(Math.random() * (max-min) + min);
    var undefined = 100 - (successful + failed);
    var evaluationResults = {successful, failed, undefined};
    console.log("evaluationResults logged:", evaluationResults)
    const response = await saveEvaluationResultsToDB(selectedDataset, llms, promptText, evaluationResults)
    if (response) {
        res.status(200).json(evaluationResults)
    } else {
        res.status(500).json({ error: "Something went wrong while saving the data to database." })
    }
  } catch (e) {
    console.error("Error while trying to upload a file\n", e);
    res.status(500).json({ error: 'Something went wrong.' })
  }
}
  