// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { getAllDatasets } from "../lib/getAllDatasets";

export default async function handler(req, res) {
  try{
    const response = await getAllDatasets();
    if (response) {
        // console.log("response:", response)
        res.status(200).json({ datasets: response})
    } else {
        res.status(500).json({ error: "Something went wrong while fetching the data from database." })
    }
  } catch (e) {
    console.error("Error while trying to fetch the data\n", e);
    res.status(500).json({ error: 'Something went wrong.' })
  }
}
  