// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import { stat, mkdir, writeFile } from "fs/promises"
import { join } from "path"
import { faker } from '@faker-js/faker';
import {saveUploads} from "../lib/saveUploadsToDB";

export default async function handler(req, res) {
    console.log("file is:", req.body)
    const relativeUploadDir = `/uploads/`;
    const path = join(process.cwd(), "public", relativeUploadDir);

  try {
    await stat(path, (err) => {
        if (err) throw err;
    });
  } catch (e) {
    if (e.code === "ENOENT") {
      await mkdir(path, { recursive: true });
    } else {
      console.error("Error while trying to create directory when uploading a file\n", e);
      res.status(500).json({ error: "Something went wrong while trying to create directory when uploading a file" })
    }
  }

  try {
    const filename = `${faker.animal.bird().replaceAll("'", "").replaceAll(" ", "")}-${faker.string.numeric({length: { min: 5, max: 10 }})}.csv`;
    await writeFile(`${path}/${filename}`, req.body, async (err) => {
        if (err) throw err;
    });
    const response = await saveUploads(filename, `${path}${filename}`);
    if (response) {
        res.status(200).json({ message: `${filename} is saved successfully.` })
    } else {
        res.status(500).json({ error: "Something went wrong while saving the data to database." })
    }
  } catch (e) {
    console.error("Error while trying to upload a file\n", e);
    res.status(500).json({ error: 'Something went wrong.' })
  }
}
  