import Head from 'next/head'
import Image from 'next/image'
import { Inter } from 'next/font/google'
import styles from '@/styles/Home.module.css'
import { useState } from 'react'

const inter = Inter({ subsets: ['latin'] })

export default function FileUploads() {

  const [file, setFile] = useState("");
  const [successResponse, setSuccessResponse] = useState("");
  const [errorResponse, setErrorResponse] = useState("")
  const [loading, setLoading] = useState(false);

  const uploadFile = async(e) => {
    e.preventDefault();
    console.log("file upload clicked!", file);
    setLoading(true);
    setErrorResponse("")
    setSuccessResponse("")
    // const file = e.target.file;
    fetch('/api/upload', {
      method: 'POST',
      headers: { 'content-type': file?.type || 'application/octet-stream' },
      body: file,
    }).then(async (res) => {
      if (res.status === 200) {
        const response = await res.json();
        console.log(response)
        setSuccessResponse(response.message)
        setLoading(false)
      } else {
        const response = await res.json();
        console.error(response)
        setErrorResponse(response.error)
        setLoading(false)
      }
    })
  }

  return (
    <>
      <div>
        <div className={styles.grid}>
          <div
            className={styles.card}
          >
            <h2>
              File Upload
            </h2>
            <p>
              Upload your datasets here.
            </p>
            <form className={styles.fileUploadSection} onSubmit={(e) => uploadFile(e)}>
              <input type='file' accept='text/csv' id="file" name="file" onChange={(e) => setFile(e.target.files?.[0])} style={{marginLeft:"25%"}}></input>
              <button type='submit' className={styles.submitFileUpload} disabled={loading}>Upload</button>
            </form>
            {successResponse && (
              <p style={{color:"green"}}>{successResponse}</p>
            )}
            {errorResponse && (
              <p style={{color:"red"}}>{errorResponse}</p>
            )}
          </div>
        </div>
      </div>
    </>
  )
}
